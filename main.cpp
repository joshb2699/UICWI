#include <QApplication>
#include "tabdialog.h"

int main(int argc, char *argv[])
{
  int code = 0;
  do 
  {
    QApplication app(argc, argv);
    GitInterface gitinterface;
    gitinterface.show();


    code = app.exec();
  }
  while(code == 1337);

  return code;
}
