#include "configtab.h"
#include "varcontainer.h"
#include "gitpp7.h"
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <iostream>
#include <QScrollArea>
#include <QString>
#include <QDebug>
#include <QSizePolicy>
#include <sstream>
#include <vector>
#include <algorithm>
#include <QPixmap>

ConfigTab::ConfigTab(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *rightColumn = new QVBoxLayout();
    rightColumn->setSizeConstraint(QLayout::SetFixedSize);
    QLabel *changeVarLabel = new QLabel("Change config variable:");
    currentVarLabel = new QLabel("");

    QHBoxLayout *oldVarLayout = new QHBoxLayout();
      oldLabel = new QLabel("old value:");
      QLabel *subvarLabel = new QLabel();
      oldVarLayout->addWidget(oldLabel);
      oldVarLayout->addWidget(subvarLabel);

    QVBoxLayout *newVarLayout = new QVBoxLayout();
      QLabel *newLabel = new QLabel("new value:");
      newVarInput = new QLineEdit();
      confirmButton = new QPushButton("Confirm");
      confirmButton->setEnabled(false);
      newVarLayout->addWidget(newLabel);
      newVarLayout->addWidget(newVarInput);
      newVarLayout->addWidget(confirmButton);

    QVBoxLayout *warningMessage = new QVBoxLayout();
      QHBoxLayout *topHalf = new QHBoxLayout();
        warningIMG = new QLabel("");
        QPixmap Warnpix("./icons/warningIcon.png");
        warningIMG->setPixmap(Warnpix.scaled(50,50));
        //warningIMG->setPixmap();
        //QLabel *warningLabel1 = new QLabel("This error");
        topHalf->addWidget(warningIMG);
        //topHalf->addWidget(warningLabel1);
      warningLabel2 = new QLabel("This error can be changed. Error messages will wrap to the next line and should be returned from a validation function which uses exceptions to discern.");
      //warningLabel2 = new QLabel("");
      warningLabel2->setWordWrap(true);
      warningMessage->addLayout(topHalf);
      warningMessage->addWidget(warningLabel2);
      warningMessage->setEnabled(false);

    rightColumn->addWidget(changeVarLabel);
    rightColumn->addWidget(currentVarLabel);
    rightColumn->addLayout(oldVarLayout);
    rightColumn->addLayout(newVarLayout);
    rightColumn->addLayout(warningMessage);

    //initialise the left column
    QVBoxLayout *leftColumn = new QVBoxLayout();

    QHBoxLayout *searchBox = new QHBoxLayout();
      varSearch = new QLineEdit();
      searchButt = new QPushButton();
      QPixmap searchPix("./icons/searchIcon.png");
      QIcon buttonIcon(searchPix.scaled(30,30));
      searchButt->setIcon(buttonIcon);
      searchButt->setFixedWidth(25);
      searchBox->addWidget(varSearch);
      searchBox->addWidget(searchButt);
      searchBox->addStretch(1);

    configScroll = new QScrollArea();

    configs = new QVBoxLayout();
    initConfigVars(configs); // pull the config variables from the git.config file and parse them into the layout
    //configs->setContentsMargins(1,1,1,1);

    dummyWidget = new QWidget(this);
    dummyWidget->setLayout(configs);
    dummyWidget->setMinimumWidth(dummyWidget->sizeHint().width()+30);
    configScroll->setWidget(dummyWidget);
    //configScroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    leftColumn->addLayout(searchBox);
    //configScroll->setMinimumWidth(dummyWidget->minimumSizeHint().width());
    leftColumn->addWidget(configScroll);

    newVarInput->setEnabled(false);

    layout = new QHBoxLayout();

    layout->addLayout(leftColumn);
    layout->addLayout(rightColumn);

    setLayout(layout);

    connect(confirmButton, SIGNAL(clicked()),
            this, SLOT(changeVariable()));

    //connect(searchButt, SINGAL(clicked()),
    //        this, SLOT(searchVariables()));

            //git commit -m 'now fully working on the dec-10 computers (well, not right now because I'm in the middle of adding the final feature). The git interaction is working now.'

}

void ConfigTab::setRightLayout(varContainer* origin) {
    currentVarContainer = origin;
    if(newVarInput->isEnabled() == false) {
      newVarInput->setEnabled(true);
      confirmButton->setEnabled(true);
    }
    QString newText = QString::fromStdString(origin->_fullVarName);
    currentVarLabel->setText(newText);
    std::string valStr = "old: ";
    QString valText = QString::fromStdString(valStr + origin->_varValue);
    oldLabel->setText(valText);
}

void ConfigTab::changeVariable() {

    int oldWidth = currentVarContainer->varLabel->width();

    /*
    qDebug() << "currentVarContainer->varLabel->width() = " << currentVarContainer->varLabel->width() << ", hint = " << currentVarContainer->varLabel->sizeHint().width();
    qDebug() << "currentVarContainer->width = " << currentVarContainer->width() << ", hint = " << currentVarContainer->sizeHint().width();
    qDebug() << "dummyWidget->width = " << dummyWidget->width() << ", hint = " << dummyWidget->sizeHint().width();
    qDebug() << "configScroll->width = " << configScroll->width() << ", hint = " << configScroll->sizeHint().width();
    qDebug() << "==================";
    */

    QString newVar = newVarInput->text();
    std::string cppstring = newVar.toUtf8().constData();
    std::string subStr = currentVarContainer->_fullVarName.substr(currentVarContainer->_fullVarName.find(".")+1);

    //if invalid, display warning

    /*if (validityCheck(subStr, cppstring).compare("none")) {
        warningLabel2->setText(QString::fromStdString(validityCheck(subStr, cppstring)));
        QPixmap pix("./images/warningSymbol.png");
        warningIMG->setPixmap(pix.scaled(50,50));
        return;
    }*/
    try {
        GITPP::REPO r;
        auto c=r.config();
        c[currentVarContainer->_fullVarName] = cppstring;
        //e.g. c[user.email]
    } catch (GITPP::EXCEPTION_CANT_FIND const& e) {
        qDebug() << "Warning: git repository not found, no real variable changed.";
    }

    currentVarContainer->_varValue = cppstring;
    std::string labelString = subStr + " = " + currentVarContainer->_varValue;
    QString Qvar = QString::fromStdString(labelString);
    currentVarContainer->varLabel->setText(Qvar);
    newVarInput->setText("");
    newVarInput->setEnabled(false);
    confirmButton->setEnabled(false);

    int newWidth = currentVarContainer->varLabel->sizeHint().width();
    int newDummyWidth = dummyWidget->width() + (newWidth-oldWidth);
    //qDebug() << dummyWidget->width() << "+ (" << newWidth << " - " << oldWidth << ")";
    dummyWidget->setMinimumWidth(newDummyWidth);

    //currentVarContainer->varLabel->resize(currentVarContainer->varLabel->sizeHint().width(), currentVarContainer->varLabel->height());
    //currentVarContainer->resize(currentVarContainer->sizeHint().width(), currentVarContainer->height());

    /* // debugging purposes
    qDebug() << "currentVarContainer->varLabel->width() = " << currentVarContainer->varLabel->width() << ", hint = " << currentVarContainer->varLabel->sizeHint().width();
    qDebug() << "currentVarContainer->width = " << currentVarContainer->width() << ", hint = " << currentVarContainer->sizeHint().width();
    qDebug() << "dummyWidget->width = " << dummyWidget->width() << ", hint = " << dummyWidget->sizeHint().width();
    qDebug() << "configScroll->width = " << configScroll->width() << ", hint = " << configScroll->sizeHint().width();
    */
}

bool is_bool(std::string var) {
    std::string s = var;
    int len = s.length();
    for (int i = 0; i < len; i++) {
        s[i] = tolower(s[i]);
    }
    if (!(s == "true" || s == "false" || s == "0" || s == "1")) {
        qDebug() << "False";
        return false;
    }
    return true;
}

//validtyCheck is not yet complete
std::string ConfigTab::validityCheck(std::string changingVarType, std::string newVar) { // core.*version*
    newVar = "";
    if (changingVarType == "repositoryformatversion") {
        for (int currentChar : changingVarType) {
            if (!isdigit(currentChar)) {
                return "repositoryformatversion must be an integer number";
            }
        }

    }
    if (changingVarType == "filemode"         ||
        changingVarType == "bare"             ||
        changingVarType == "logallrefupdates" ||
        changingVarType == "symlinks"         ||
        changingVarType == "ignorecase"       ){
        if (!is_bool(changingVarType))
            return "filemode must be a bool value, i.e. 'true', 'false', '1' or '0' ";
    }
    return "none";
}

void ConfigTab::initConfigVars(QVBoxLayout* layout, std::string searchStr) {
    try {
        GITPP::REPO r;
        auto c = r.config();
        varContainer *varSub = nullptr;
        std::vector<std::string> vectorOfHeads; // this whole crap is to get them to display in order
        for (auto i : c) {
            //find the head of the variable
            std::stringstream ss (i.name());
            std::string head;
            getline(ss, head, '.');
            //std::cout << head << "\n";
            if (vectorOfHeads.size() == 0 || (std::find(vectorOfHeads.begin(), vectorOfHeads.end(), head) == vectorOfHeads.end())) {
              //std::cout << "that's true!\n";
              // if the head string is not in the vector for holding head strings
              vectorOfHeads.push_back(head); // add it to the vector (automatically assigns more memory)
            }
        }
        for (auto currentHead : vectorOfHeads) {
            QString QHead = QString::fromStdString(currentHead + ": ");
            QLabel *varHeadLabel = new QLabel(QHead);
            varHeadLabel->setStyleSheet("border-top-width: 1px; border-top-style: solid; border-radius: 0px;");
            layout->addWidget(varHeadLabel);
            for (auto i : c) {
                GITPP::CONFIG::ITEM n = c[i.name()];
                //find the head of the variable
                std::stringstream ss (i.name());
                std::string head;
                getline(ss, head, '.');
                std::string subStr = i.name().substr(i.name().find(".")+1);
                if ((currentHead == head) &&
                   ((subStr.find(searchStr)!=std::string::npos) || (searchStr == ""))) {
                    varSub = new varContainer(this, i.name(), n.value(), this);
                    //std::cout << "i.name() = " << i.name() << " | n.value() = " << n.value() << "\n";
                    layout->addWidget(varSub);
                }
            }
        }
    } catch (GITPP::EXCEPTION_CANT_FIND const& e) {
        qDebug() << "Cannot initialise config variables: No .git found in the current directory";
    }
}

void ConfigTab::searchVariables() {
  delete configs;
  //initConfigVars(configs, varSearch.text().toUtf8().constData());
}
