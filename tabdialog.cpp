#include <QtWidgets>
#include "gitpp7.h"
#include "tabdialog.h"
#include "checkouttab.h"
#include "foldertab.hpp"
#include "keywordsearch.hpp"
#include "configtab.h"
#include "commit.h"



GitInterface::GitInterface(QWidget *parent)
    : QDialog(parent)
{
  setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint
    | Qt::WindowCloseButtonHint);

  tabWidget = new QTabWidget;

  tabWidget->addTab(new FolderTab(), tr("Select Repo"));
  tabWidget->addTab(new CheckoutTab(), tr("Branch navigation"));
  tabWidget->addTab(new CommitHistoryTab(), tr("Commit History"));
  tabWidget->addTab(new KeywordSearch(), tr("Keyword Search"));
  tabWidget->addTab(new ConfigTab(), tr("Config"));


  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(tabWidget);
  setLayout(mainLayout);

  setWindowTitle(tr("Git interface"));
}
