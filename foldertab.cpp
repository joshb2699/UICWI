#include <QtWidgets>
#include <unistd.h>
#include <iostream>

#include "gitpp7.h"
#include "foldertab.hpp"

FolderTab::FolderTab(QWidget *parent)
  : QWidget(parent)
{
  branchListBox = new QListWidget;

  auto select_button = new QPushButton("Select Repo", this);
  select_button->setFixedSize(QSize(400, 60));
  connect(select_button, SIGNAL (released()), this, SLOT (dialog()));


  //create layout
  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(select_button);
  setLayout(layout);
}

void FolderTab::dialog()
{
  QString dir = QFileDialog::getExistingDirectory(0, ("Select Output Folder"), QDir::currentPath());

  QByteArray ba = dir.toLocal8Bit();

  char buf[PATH_MAX];
  getcwd(buf, sizeof(buf));
  chdir(ba.data());

  try
  {
    GITPP::REPO r;
  }
  catch (GITPP::EXCEPTION_CANT_FIND)
  {
    QMessageBox msgBox;
    msgBox.setText("There is no git repo here");
    msgBox.setInformativeText("Do you want to create one?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();

    switch (ret) {
      case QMessageBox::No:
        chdir(buf);
        return;
        break;
      case QMessageBox::Yes:
        GITPP::REPO(GITPP::REPO::_create, ".");
        break;
      default:
        chdir(buf);
        return;
        break;
    }
  }

  qApp->exit(1337);
}
