#ifndef CONFIGTAB_H
#define CONFIGTAB_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <iostream>
#include <QScrollArea>
#include <QString>
#include <QDebug>
#include <QSizePolicy>
#include <sstream>
#include <vector>
#include <algorithm> // to call find on vectors
#include <QPixmap>  // for icons

#include "varcontainer.h"
#include "gitpp7.h"

class ConfigTab : public QWidget
{
    Q_OBJECT
public:
    explicit ConfigTab(QWidget *parent = nullptr);

    std::string validityCheck(std::string changingVarType, std::string newVar);
    void initConfigVars(QVBoxLayout* layout, std::string searchStr = "");
    void searchVariables(std::string searchString);

private:
    // only members that needs to be changed as the program runs are included here
    // the rest are declared inside the constructor
    QHBoxLayout *layout;
    QLineEdit *newVarInput;
    QLabel *currentVarLabel;
    QLabel *oldLabel;
    varContainer *currentVarContainer = nullptr;
    QScrollArea *configScroll;
    QWidget *dummyWidget;
    QLabel *warningLabel2;
    QPushButton *confirmButton;
    QPushButton *searchButt;
    QLabel *warningIMG;
    QVBoxLayout *configs = nullptr;
    QLineEdit *varSearch;

signals:

public slots:
    void setRightLayout(varContainer*);
    void changeVariable();
    void searchVariables();
};

#endif // CONFIGTAB_H
